import 'dart:io';

void main(List<String> args){
  // SOAL 1
  // print("Apakah Anda ingin menginstall aplikasi? Pilihan jawaban: Y/T");
  // String? jawaban = stdin.readLineSync();

  // if (jawaban == 'Y' || jawaban == 'y') {
  //   print("Anda akan menginstall aplikasi dart");
  // } else if (jawaban == 'T' || jawaban == 't'){
  //   print("Aborted");
  // } else {
  //   print("Pilihan jawaban hanya Y/T");
  // }

  // SOAL 2
  // print("Nama:");
  // String? nama = stdin.readLineSync();
  // print("Peran:");
  // String? peran = stdin.readLineSync();

  // if (nama == ""){
  //   nama = null;
  // }

  // if (peran == ""){
  //   peran = null;
  // }

  // if (nama == null){
  //   print("Nama harus diisi!");
  // } else if (nama != null && peran == null){
  //   print("Halo "+nama+", pilih peranmu untuk memulai game!");
  // } else if (nama != null && peran == 'penyihir'){
  //   print("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
  // } else if (nama != null && peran == 'guard'){
  //   print("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf");
  // } else if (nama != null && peran == 'werewolf'){
  //   print("Halo Werewolf "+nama+", kamu akan memakan mangsa setiap malam!");
  // }

  // SOAL 3
  // var senin =  "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.";
  // var selasa = "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.";
  // var rabu = "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.";
  // var kamis = "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.";
  // var jumat = "Hidup tak selamanya tentang pacar.";
  // var sabtu = "Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.";
  // var minggu = "Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.";

  // print("Quotes hari apa?");
  // String? hari = stdin.readLineSync();

  // if (hari == 'senin'){
  //   print(senin);
  // } else if (hari == 'selasa'){
  //   print(selasa);
  // } else if (hari == 'rabu'){
  //   print(rabu);
  // } else if (hari == 'kamis'){
  //   print(kamis);
  // } else if (hari == 'jumat'){
  //   print(jumat);
  // } else if (hari == 'sabtu'){
  //   print(sabtu);
  // } else if (hari == 'minggu'){
  //   print(minggu);
  // } else {
  //   print("Masukkan hari senin s/d minggu.");
  // }

  // SOAL 4
  var tanggal = 21;
  var bulan = 1;
  var tahun = 1945;
  var bulanString = '';

  if (bulan == 1){
    bulanString = 'Januari';
  } else if (bulan == 2){
    bulanString = 'Februari';
  } else if (bulan == 3){
    bulanString = 'Maret';
  } else if (bulan == 4){
    bulanString = 'April';
  } else if (bulan == 5){
    bulanString = 'Mei';
  } else if (bulan == 6){
    bulanString = 'Juni';
  } else if (bulan == 7){
    bulanString = 'Juli';
  } else if (bulan == 8){
    bulanString = 'Agustus';
  } else if (bulan == 9){
    bulanString = 'September';
  } else if (bulan == 10){
    bulanString = 'Oktober';
  } else if (bulan == 11){
    bulanString = 'November';
  } else if (bulan == 12){
    bulanString = 'Desember';
  } else {
    bulanString = 'error';
  }

  if (bulanString == 'error'){
    print("Masukkan bulan Januari s/d Desember.");
  } else {
    print(tanggal.toString()+" "+bulanString+" "+tahun.toString());
  }

} 

